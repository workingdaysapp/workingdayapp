package com.DeSPo.workingday;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

public class LoginActivity extends Activity {
	private Dialog Dialog_DevelopmentLogoScreen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.singin);

		final Button btnCreateAccount = (Button) findViewById(R.id.btn_create_an_account);
		btnCreateAccount.setClickable(false);
		btnCreateAccount.setVisibility(View.GONE);

		CheckBox cB_AgreePolicy = (CheckBox) findViewById(R.id.checkBox_agree_to_policy);

		cB_AgreePolicy.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()) {
					btnCreateAccount.setVisibility(View.VISIBLE);
					btnCreateAccount.setClickable(true);
					Log.i("tag", "clickable");
				} else {
					btnCreateAccount.setVisibility(View.GONE);
					btnCreateAccount.setClickable(false);
					Log.i("tag", "not clickable");
				}
			}

		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.main, menu);
		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.item_about) {
			ShowDialog_DevelopmentLogoScreen();

		}
		return true;

	}

	@SuppressWarnings("static-access")
	private void ShowDialog_DevelopmentLogoScreen() {

		Dialog_DevelopmentLogoScreen = new Dialog(this);

		Dialog_DevelopmentLogoScreen.requestWindowFeature(Dialog_DevelopmentLogoScreen.getWindow().FEATURE_NO_TITLE);
		Dialog_DevelopmentLogoScreen.setContentView(R.layout.development_logo_screen);

		Dialog_DevelopmentLogoScreen.setCancelable(true);

		Dialog_DevelopmentLogoScreen.show();
	}
}
